import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

interface verifyButtonsInterface {
  "welcome": (inputs: fieldsInterface) => boolean;
  "contacts": (inputs: fieldsInterface) => boolean;
  "reason": (inputs: fieldsInterface) => boolean;
}

interface fieldsInterface {
  "name": string;
  "contactWay": string;
  "topic": string;
}

interface pagesInterface {
  "welcome": boolean,
  "contacts": boolean,
  "reason": boolean
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  submitStatus = false;
  failed = false;
  fields: fieldsInterface = {
    "name": "",
    "contactWay": "",
    "topic": "",
  };

  pages: pagesInterface = {
    "welcome": true,
    "contacts": false,
    "reason": false
  }

  private verifyButtons: verifyButtonsInterface = {
    "welcome": (inputs: fieldsInterface) => inputs["name"].length > 0,
    "contacts": (inputs: fieldsInterface) => inputs["contactWay"].length > 0,
    "reason": (inputs: fieldsInterface) => inputs["topic"].length > 0
  };

  constructor() {
  }

  ngOnInit(): void {
  }

  onKey(field: string, event: any) {
    // @ts-ignore
    this.fields[field] = (event.target as HTMLInputElement).value;
  }

  onButton(newPage: string, oldPage: string, direction: string, event: any) {
    // @ts-ignore
    console.log(this.verifyButtons[oldPage](this.fields))
    // @ts-ignore
    if (this.verifyButtons[oldPage](this.fields) || direction === "previous") {
      // @ts-ignore
      this.pages[oldPage] = false;
      // @ts-ignore
      this.pages[newPage] = true;
      console.log(newPage)
    }
  }

  async submit() {
    const baseURL = "https://discord.com/api/webhooks/1009181921118539787/LjxnBsDOJtdgQc4548S8KNuADqwNm2LAxQkcqAwj01H5MBV1HptwcSa2IT-F5z1d3y1f"

    // opening up the request
    const request = new XMLHttpRequest();
    request.open("POST", baseURL);
    request.addEventListener('error', event => this.failed = true)

    // Preparing data
    request.setRequestHeader("Content-type", "application/json");
    const params = {
      username: this.fields["name"],
      avatar_url: "",
      content: `**Contact:** ${this.fields["contactWay"]}\n**Reason:** ${this.fields["topic"]}`
    };
    try {
      request.send(JSON.stringify(params));
    } catch (error) {
      console.error(error);
      this.failed = true
    }

    // Waiting for Eventlistener to run
    setTimeout(() => this.submitStatus = !this.failed, 300)
  }
}
