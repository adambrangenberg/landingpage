import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css']
})
export class LinksComponent implements OnInit {
  public links: string[] = [
    "Codeberg ⛰",
    "GitLab 🦊",
    "GitHub (inactive) 😺",
    "Discord 💬",
    "Resources 🧠",
    "Blog 📓",
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
