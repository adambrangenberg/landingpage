import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-links-router',
  template: `Opening {{redirectURL}} in a new tab...`,
  styles: ['']
})

export class LinksRouterComponent implements OnInit {
  redirectURL: string = "";

  private availableURLs: Record<string, string> = {
    "codeberg": "https://codeberg.org/adambrangenberg",
    "gitlab": "https://gitlab.com/adambrangenberg",
    "github": "https://github.com/TigerbyteDev",
    "blog": "https://medium.com/@adamwarvergeben",
    "discord": "https://discord.gg/AmUtjm3cjW",
    "resources": "https://resources.tigerbyte.dev"
  };

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  async ngOnInit(): Promise<void> {
    let link: string = "";

    // Getting the parameter link out of the URL
    this.route.params.subscribe(params => {
      link = params["link"];
    });

    // Redirecting to the requested URL, if existent
    this.redirectURL = this.availableURLs[link] ?? "";

    // If the URL is valid redirect, else show 404
    if (Boolean(this.redirectURL)) {
      window.open(this.redirectURL, "_blank");
      await this.router.navigate(["links"]);
      return;
    }

    await this.router.navigate(["not-found"]);
  }
}
