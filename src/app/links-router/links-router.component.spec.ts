import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LinksRouterComponent } from './links-router.component';

describe('LinksRouterComponent', () => {
  let component: LinksRouterComponent;
  let fixture: ComponentFixture<LinksRouterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinksRouterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LinksRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
