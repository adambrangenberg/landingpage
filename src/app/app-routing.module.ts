import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {ContactComponent} from "./contact/contact.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {LinksComponent} from "./links/links.component";
import {LinksRouterComponent} from "./links-router/links-router.component";
import {ImprintComponent} from "./imprint/imprint.component";

const routes: Routes = [
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "links",
    component: LinksComponent,
  },
  {
    path: "links/:link",
    component: LinksRouterComponent
  },
  {
    path: "contact",
    component: ContactComponent
  },
  {
    path: "imprint",
    component: ImprintComponent
  },

  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
