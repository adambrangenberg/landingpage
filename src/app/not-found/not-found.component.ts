import {Component} from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <div class="center_vertical">
      <div class="center_horizontal">
        <img src="https://http.cat/404" alt="404 Not Found" class="responsive">
      </div>
    </div>
  `,
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent {
}
