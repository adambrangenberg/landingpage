import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <div class="center">
      <div class="mainTitle">Hello! 👋</div>
      <div class="mainTitle">I am Adam. 🧒</div>
      <div class="subTitle">A person who enjoys philosophy 🤔, math 🧮 and programming 👨‍💻</div>
    </div>
  `,
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
