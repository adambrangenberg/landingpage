import { Component } from '@angular/core';
import {faHomeUser, faLink, faMessage} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  home = faHomeUser
  links = faLink
  contact= faMessage
  title = 'landingpage';
  date = new Date().getFullYear() + "  ";
}
